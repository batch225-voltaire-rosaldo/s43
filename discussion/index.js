let posts = []; //storage
let count = 1; // id

// id: 1
// id: 2

// add post data

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });

    count++;

    console.log(posts);
    showPosts(posts);
    alert('Successfully added');

});

// Show posts

const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries += `<div id="post-${post.id}">
            <h3 id="post-title-${post.id}"> ${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
            </div>
            <button onclick="editPost('${post.id}')" id="btn-edit-${post.id}">Edit</button>
            <button onclick="deletePost('${post.id}')" id="btn-del-${post.id}">Delete</button>
        `
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit posts

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;
    
    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

};

// Update posts // Backend

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault();

    for (let i=0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id).value is a String.
        // Therefore it is neccessary to convert the Number to a String first
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            // Logic
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
        }
    }
    showPosts(posts);
    alert('Successfully updated.');
});

// Activity s43
// Delete posts

const deletePost1 = (id) => {

    for (let i=0; i < posts.length; i++) {

        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            // Logic
            posts.splice(i, 1);
        }
    }

    // showPosts(posts);
    alert('Successfully deleted.');

};

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() == id) {
            return post;
        }
    });
    document.querySelector(`#post-${id}`).remove();
    document.querySelector(`#btn-edit-${id}`).remove();
    document.querySelector(`#btn-del-${id}`).remove();
}


